package sn.ahmedkane.flitght.service;


import sn.ahmedkane.flitght.persistance.entities.Meta;
import sn.ahmedkane.flitght.web.dto.MetaDto;

public interface IMetaService {
    Meta createNew(String resourceType);

    MetaDto parse(Meta meta);

    Meta findByExternalId(String linkedMeta);

    MetaDto save(Meta meta);

}
