package sn.ahmedkane.flitght.service.imp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import sn.ahmedkane.flitght.persistance.entities.Airline;
import sn.ahmedkane.flitght.persistance.repository.AirlineRepository;
import sn.ahmedkane.flitght.service.IAirlineService;
import sn.ahmedkane.flitght.service.IMetaService;
import sn.ahmedkane.flitght.utils.StatusResponse;
import sn.ahmedkane.flitght.web.dto.AirlineRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirlineTravelPayoutsDto;
import sn.ahmedkane.flitght.web.dto.SearchAirlineDto;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirline;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirlineTravelPayouts;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportRapidapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@Transactional
public class AirlineServiceImp implements IAirlineService {

    private List<AirlineRapidapiDto> airlineData = new ArrayList<>();
    private List<AirlineTravelPayoutsDto> airlineDatayTravelPayouts = new ArrayList<>();


    @Autowired
    private AirlineRepository airlineRepository;

    @Autowired
    private IMetaService metaService;


    @Override
    public ResponseAirline getAirlineFromRapidapi() {

        try {
            AsyncHttpClient client = new DefaultAsyncHttpClient();
            CompletableFuture<Response> response =
                    client.prepare("GET", "https://iata-and-icao-codes.p.rapidapi.com/airlines")
                            .setHeader("x-rapidapi-host", "iata-and-icao-codes.p.rapidapi.com")
                            .setHeader("x-rapidapi-key", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                            .execute()
                            .toCompletableFuture();

            ObjectMapper mapper = new ObjectMapper();

            response.thenAccept(data-> {
                        try {
                            setDataAirline(mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructCollectionType(List.class, AirlineRapidapiDto.class)));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }

            ).join();
            client.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseAirline airportRapidapi = new ResponseAirline();
        airportRapidapi.setSize(this.airlineData.size());
        airportRapidapi.setPage(0);
        airportRapidapi.setContents(this.airlineData);

        return airportRapidapi;
    }

    @Override
    public ResponseAirlineTravelPayouts getAirlineFromTravelPayouts() {
        try {
            AsyncHttpClient client = new DefaultAsyncHttpClient();
            CompletableFuture<Response> response =
                    client.prepare("GET", "http://api.travelpayouts.com/data/en/airlines.json")
                            .execute()
                            .toCompletableFuture();

            ObjectMapper mapper = new ObjectMapper();

            response.thenAccept(data-> {
                        try {
                            setDataAirlineTravelPayouts(mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructCollectionType(List.class, AirlineTravelPayoutsDto.class)));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }

            ).join();
            client.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseAirlineTravelPayouts airportTravelPayouts = new ResponseAirlineTravelPayouts();
        airportTravelPayouts.setSize(this.airlineData.size());
        airportTravelPayouts.setPage(0);
        airportTravelPayouts.setContents(this.airlineDatayTravelPayouts);

        return airportTravelPayouts;
    }

    @Override
    public boolean saveAllFromRapidapiBody(List<AirlineRapidapiDto> airlineRapidapiDtoList) {
        Airline airlineCreateObj = null;
        boolean result = false;

        if(airlineRapidapiDtoList.isEmpty()){
            throw new RestClientException("La liste des Aéroport doit contenir au moins un élements");
        }
        for(AirlineRapidapiDto dto: airlineRapidapiDtoList){
            airlineCreateObj = new Airline();
            if(Objects.nonNull(dto.getIata_code())){
                airlineCreateObj.setIataCode(dto.getIata_code());
            }

            if(Objects.nonNull(dto.getIcao_code())){
                airlineCreateObj.setIcaoCode(dto.getIcao_code());
            }

            if(Objects.nonNull(dto.getName())){
                airlineCreateObj.setName(dto.getName());
            }

            airlineCreateObj.setLinkedMeta(this.metaService.createNew(Airline.class.getSimpleName()));

            if(this.airlineRepository.save(airlineCreateObj) != null){
                result = true;
            }
        }

        return result;
    }

    @Override
    public boolean saveAllFromTravelPayoutBody(List<AirlineTravelPayoutsDto> airlineTravelPayoutsDtos) {
        Airline airlineCreateObj = null;
        boolean result = false;


        if(airlineTravelPayoutsDtos.isEmpty()){
            throw new RestClientException("La liste des Aeroport doit contenir au moins un elements");
        }
        for(AirlineTravelPayoutsDto dto: airlineTravelPayoutsDtos){
            airlineCreateObj = new Airline();
            if(Objects.nonNull(dto.getCode())){
                airlineCreateObj.setIataCode(dto.getCode());
            }

            if(Objects.nonNull(dto.getName())){
                airlineCreateObj.setName(dto.getName());
            }

            airlineCreateObj.setLinkedMeta(this.metaService.createNew(Airline.class.getSimpleName()));

            if(this.airlineRepository.save(airlineCreateObj) != null){
                result = true;
            }
        }

        return result;
    }

    @Override
    public StatusResponse saveAllFromRapidapi() {
        StatusResponse statusResponse = new StatusResponse();
        List<AirlineRapidapiDto> airlineRapidapiDtos = this.getAirlineFromRapidapi().getContents();
        if(this.saveAllFromRapidapiBody(airlineRapidapiDtos)){
            statusResponse.setCode(200);
            statusResponse.setMessage("Les aéroports retournés par l'api rapidapi ont été enrégistrés avec succès");
        }else{
            statusResponse.setCode(403);
            statusResponse.setMessage("Erreur au moment de l'enregistrement des aéroport provenant de l'api Rapidapi");
        }
        return statusResponse;
    }


    @Override
    public StatusResponse saveAllFromTravelPayouts() {
        StatusResponse statusResponse = new StatusResponse();
        List<AirlineTravelPayoutsDto> airlineTravelPayoutsDtos = this.getAirlineFromTravelPayouts().getContents();
        if(this.saveAllFromTravelPayoutBody(airlineTravelPayoutsDtos)){
            statusResponse.setCode(200);
            statusResponse.setMessage("Les aéroports retournés par l'api rapidapi ont été enrégistrés avec succès");
        }else{
            statusResponse.setCode(403);
            statusResponse.setMessage("Erreur au moment de l'enregistrement des aéroport provenant de l'api Rapidapi");
        }
        return statusResponse;
    }

    @Override
    public ResponseAirline getAll(int page , int size) {
        ResponseAirline response = new ResponseAirline();
        if(size == 0) size = 25;
        response.setPage(page);

        response.setContents(AirlineRapidapiDto.parse(this.airlineRepository.findAll(PageRequest.of(page,size)).getContent()));
        response.setSize(response.getContents().size());
        return response;
    }

    @Override
    public ResponseAirlineTravelPayouts getAllFromTravelPayouts(int page, int size) {
        ResponseAirlineTravelPayouts response = new ResponseAirlineTravelPayouts();
        if(size == 0) size = 25;
        response.setPage(page);

        response.setContents(AirlineTravelPayoutsDto.parse(this.airlineRepository.findAll(PageRequest.of(page,size)).getContent()));
        response.setSize(response.getContents().size());
        return response;
    }

    @Override
    public Airline getByExternalId(String externalId) {
        return this.airlineRepository.findByLinkedMetaExternalId(externalId);
    }

    public void setDataAirline(List<AirlineRapidapiDto> airlines){
       this.airlineData = airlines;
    }

    public void setDataAirlineTravelPayouts(List<AirlineTravelPayoutsDto> airlines){
        this.airlineDatayTravelPayouts = airlines;
    }

    @Override
    public List<AirlineRapidapiDto> searchByIata(String codeIata) {
        return AirlineRapidapiDto.parse(this.airlineRepository.findByIataCodeContainingIgnoreCase(codeIata));
    }

    @Override
    public List<AirlineRapidapiDto> searchByIcao(String codeIcao) {
        return AirlineRapidapiDto.parse(this.airlineRepository.findByIataCodeContainingIgnoreCase(codeIcao));
    }

    @Override
    public List<AirlineRapidapiDto> searchByName(String name) {
        return AirlineRapidapiDto.parse(this.airlineRepository.findByNameContainingIgnoreCase(name));
    }

    @Override
    public ResponseAirline searchByParams(SearchAirlineDto dtoSearch, int page,int size) {
        if(size == 0 ) size = 25;
        ResponseAirline responseAirline = new ResponseAirline();
        responseAirline.setPage(page);

        List<AirlineRapidapiDto> result = new ArrayList<>();

             if(Objects.nonNull(dtoSearch.getIataCode()) && Objects.nonNull(dtoSearch.getName()) && Objects.nonNull(dtoSearch.getIcaoCode())){
                // TODO : Le filtre pour 3 paramètres
                 result = this.getAll(page,size).getContents().stream().filter(airline ->
                         airline.getIata_code().equals(dtoSearch.getIataCode())
                         && airline.getIcao_code().equals(dtoSearch.getIcaoCode())
                         && airline.getName().equals(dtoSearch.getName()))
                         .collect(Collectors.toList());
             }else{
                 if(Objects.nonNull(dtoSearch.getIataCode()) && Objects.nonNull(dtoSearch.getName())){
                     // TODO : Le filtre pour 2 paramètres iata-code et name
                     result = this.getAll(page,size).getContents().stream().filter(airline ->
                             airline.getIata_code().equals(dtoSearch.getIataCode())
                                     && airline.getName().equals(dtoSearch.getName()))
                             .collect(Collectors.toList());
                 }else{
                     if(Objects.nonNull(dtoSearch.getIataCode()))
                         result =  this.searchByIata(dtoSearch.getIataCode());
                     else
                         result =  this.searchByName(dtoSearch.getName());

                 }
             }

             responseAirline.setSize(result.size());
             responseAirline.setContents(result);

        return responseAirline;
    }
}
