package sn.ahmedkane.flitght.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sn.ahmedkane.flitght.service.IAirportService;
import sn.ahmedkane.flitght.utils.StatusResponse;
import sn.ahmedkane.flitght.web.dto.AirportTravelPayoutsDto;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportDistanceRapidapi;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportRapidapi;
import sn.ahmedkane.flitght.web.dto.response.ResponseTravelPayoutsdapi;

@RestController(value = "/api")
public class AirportController {

    @Autowired
    private IAirportService airportService;

    @GetMapping(path = "airportByIata/iata_code={iata}" )
    public ResponseAirportRapidapi getByIataCodeFromRapidapi(@PathVariable String iata){
        return this.airportService.getByIataCodeFromTravelPayouts(iata);
    }

    @GetMapping(path = "airportFromTravelPayouts")
    public ResponseTravelPayoutsdapi getAirportsFromRaTravelPayouts(){
        return this.airportService.getAllAirportFromTravelPayouts();
    }


    @PostMapping(path = "airport/add")
    public StatusResponse addAirportFromTravelPayouts(@RequestBody AirportTravelPayoutsDto dtoCreate){
        return this.airportService.create(dtoCreate);
    }

    @PostMapping(path = "airports/p=/{page}&s={size}")
    public ResponseTravelPayoutsdapi getAll(@PathVariable int page ,@PathVariable int size){
        return this.airportService.getAll(page,size);
    }

    @PostMapping(path = "/airportsFromTravelPayouts/add")
    public StatusResponse addAirportsFromTravelPayouts(){
        return this.airportService.createAirportsFromTravelPayouts();
    }

    @GetMapping(path = "airportByIataContain/q={code}" )
    public ResponseTravelPayoutsdapi searchyIataCode(@PathVariable String code){
        return this.airportService.getByIata(code);
    }

    @GetMapping(path = "airport/distanceOf2Airport/iata-1={iata1}&iata-2={iata2}")
    public ResponseAirportDistanceRapidapi distanceOf2Airports(@PathVariable String iata1, @PathVariable String iata2){
        return this.airportService.getDistanceof2Airports(iata1,iata2);
    }
}
