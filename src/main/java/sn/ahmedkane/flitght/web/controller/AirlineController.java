package sn.ahmedkane.flitght.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sn.ahmedkane.flitght.service.IAirlineService;
import sn.ahmedkane.flitght.utils.StatusResponse;
import sn.ahmedkane.flitght.web.dto.AirlineRapidapiDto;
import sn.ahmedkane.flitght.web.dto.SearchAirlineDto;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirline;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirlineTravelPayouts;

@RestController
@RequestMapping(path = "/api")
public class AirlineController {

    @Autowired
    private IAirlineService airlineService;

    @GetMapping(value = "getAirlineFromRapidapi")
    public ResponseAirline getAirlineFromRapidapi(){
      return this.airlineService.getAirlineFromRapidapi();
    }

    @PostMapping(value = "airline/addAirlinesFromRapidapi")
    public StatusResponse saveAirelineFromRapidapi(){
        return this.airlineService.saveAllFromRapidapi();
    }

    @PostMapping(value = "airline/addAirlinesFromTravelPayouts")
    public StatusResponse saveAirelineFromTravelPayouts(){
        return this.airlineService.saveAllFromTravelPayouts();
    }


    @GetMapping(path = "airlines/rapidapi/p={page}&s={size}")
    public ResponseAirline getAllRapidapi(@PathVariable int page, @PathVariable int size){
        return this.airlineService.getAll(page,size);
    }


    @GetMapping(path = "airlines/travel-payouts/p={page}&s={size}")
    public ResponseAirlineTravelPayouts getAllTravelPayouts(@PathVariable int page, @PathVariable int size){
        return this.airlineService.getAllFromTravelPayouts(page,size);
    }



    @PostMapping(path = "airlineByExternalId")
    public AirlineRapidapiDto getByExternalId(@RequestBody String externalId){
        return AirlineRapidapiDto.parse(this.airlineService.getByExternalId(externalId));
    }

    @PostMapping(path = "airlines/search")
    public ResponseAirline searchByParams(@RequestBody SearchAirlineDto dtoSearch){
        return this.airlineService.searchByParams(dtoSearch,dtoSearch.getPage(),dtoSearch.getSize());
    }

}
