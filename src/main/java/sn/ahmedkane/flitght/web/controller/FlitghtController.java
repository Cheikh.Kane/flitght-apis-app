package sn.ahmedkane.flitght.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sn.ahmedkane.flitght.service.IVoleService;
import sn.ahmedkane.flitght.web.dto.SearchDto;
import sn.ahmedkane.flitght.web.dto.VolViewDto;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class FlitghtController {

    @Autowired
    private IVoleService flitghtService;

    //@PostMapping(path = "flitght")
    @GetMapping(path = "flitght")
    public List<VolViewDto> getVols(
            //@RequestBody SearchDto dto
             ){
        SearchDto searchDto = new SearchDto();
        return this.flitghtService.getFlitghtByAirport(searchDto);
    }
}
