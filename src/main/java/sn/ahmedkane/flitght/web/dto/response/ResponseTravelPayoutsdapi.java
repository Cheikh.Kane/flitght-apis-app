package sn.ahmedkane.flitght.web.dto.response;


import sn.ahmedkane.flitght.web.dto.AirportRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirportTravelPayoutsDto;

import java.util.List;

public class ResponseTravelPayoutsdapi {
    private int page;
    private int size;
    private List<AirportTravelPayoutsDto> contents;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<AirportTravelPayoutsDto> getContents() {
        return contents;
    }

    public void setContents(List<AirportTravelPayoutsDto> contents) {
        this.contents = contents;
    }
}
