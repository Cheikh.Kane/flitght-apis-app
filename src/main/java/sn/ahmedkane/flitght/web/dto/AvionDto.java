package sn.ahmedkane.flitght.web.dto;

import java.io.Serializable;

public class AvionDto implements Serializable {

    private String model;
    private String reg;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getReg() {
        return reg;
    }

    public void setReg(String reg) {
        this.reg = reg;
    }
}
