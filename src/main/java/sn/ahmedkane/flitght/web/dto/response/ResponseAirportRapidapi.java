package sn.ahmedkane.flitght.web.dto.response;


import sn.ahmedkane.flitght.web.dto.AirportRapidapiDto;

import java.util.List;

public class ResponseAirportRapidapi {
    private int page;
    private int size;
    private List<AirportRapidapiDto> contents;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<AirportRapidapiDto> getContents() {
        return contents;
    }

    public void setContents(List<AirportRapidapiDto> contents) {
        this.contents = contents;
    }
}
