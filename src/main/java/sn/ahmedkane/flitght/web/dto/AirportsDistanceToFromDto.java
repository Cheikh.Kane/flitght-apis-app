package sn.ahmedkane.flitght.web.dto;

import java.io.Serializable;
import java.util.Map;

public class AirportsDistanceToFromDto implements Serializable {

    AirportRapidapiDto from;
    AirportRapidapiDto to;
    GreatCircleDistanceDto greatCircleDistance;
    String approxFlightTime;

    public AirportRapidapiDto getFrom() {
        return from;
    }

    public void setFrom(AirportRapidapiDto from) {
        this.from = from;
    }

    public AirportRapidapiDto getTo() {
        return to;
    }

    public void setTo(AirportRapidapiDto to) {
        this.to = to;
    }

    public GreatCircleDistanceDto getGreatCircleDistance() {
        return greatCircleDistance;
    }

    public void setGreatCircleDistance(GreatCircleDistanceDto greatCircleDistance) {
        this.greatCircleDistance = greatCircleDistance;
    }

    public String getApproxFlightTime() {
        return approxFlightTime;
    }

    public void setApproxFlightTime(String approxFlightTime) {
        this.approxFlightTime = approxFlightTime;
    }
}


class GreatCircleDistanceDto{
    private double meter;
    private double km;
    private double mile;
    private double nm;
    private double feet;

    public double getMeter() {
        return meter;
    }

    public void setMeter(double meter) {
        this.meter = meter;
    }

    public double getKm() {
        return km;
    }

    public void setKm(double km) {
        this.km = km;
    }

    public double getMile() {
        return mile;
    }

    public void setMile(double mile) {
        this.mile = mile;
    }

    public double getNm() {
        return nm;
    }

    public void setNm(double nm) {
        this.nm = nm;
    }

    public double getFeet() {
        return feet;
    }

    public void setFeet(double feet) {
        this.feet = feet;
    }
}