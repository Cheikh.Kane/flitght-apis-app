package sn.ahmedkane.flitght.web.dto;

import sn.ahmedkane.flitght.persistance.entities.Airline;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AirlineRapidapiDto implements Serializable {

    private String iata_code;

    private String name;

    private String icao_code;

    private MetaDto meta;

    public AirlineRapidapiDto(){}
    public AirlineRapidapiDto(Airline airline){
        if(Objects.nonNull(airline.getIataCode())){
            this.iata_code = airline.getIataCode();
        }
        if(Objects.nonNull(airline.getIcaoCode())){
            this.icao_code = airline.getIcaoCode();
        }
        if(Objects.nonNull(airline.getName())){
            this.name = airline.getName();
        }
        if(Objects.nonNull(airline.getLinkedMeta())){
            this.meta = MetaDto.parse(airline.getLinkedMeta());
        }
    }

    public String getIata_code() {
        return iata_code;
    }

    public void setIata_code(String iata_code) {
        this.iata_code = iata_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcao_code() {
        return icao_code;
    }

    public void setIcao_code(String icao_code) {
        this.icao_code = icao_code;
    }

    public MetaDto getMeta() {
        return meta;
    }

    public void setMeta(MetaDto meta) {
        this.meta = meta;
    }

    public static AirlineRapidapiDto parse(Airline airline){
      return new AirlineRapidapiDto(airline);
    }
    public static List<AirlineRapidapiDto> parse(List<Airline> airlines){
        return airlines.stream().map(AirlineRapidapiDto::parse).collect(Collectors.toList());
    }


}
