package sn.ahmedkane.flitght.persistance.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Ahmed Kane
 */
@Entity
@Table(name = Meta.TABLE_NAME, uniqueConstraints = {
        @UniqueConstraint(columnNames = {"external_id", "id"})})
public class Meta implements Serializable {

    public static final String TABLE_NAME = "meta";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "external_id", nullable = false, length = 255)
    private String externalId;

    @Column(name = "resource_type", nullable = false, length = 55)
    private String resourceType;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;


    @Column(name = "last_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;


    @Basic(optional = false)
    @Column(name = "created_by", nullable = false)
    private int author;

    public Meta() {
    }

    public Meta(Integer id) {
        this.id = id;
    }

    public Meta(Integer id, String externalId, String resourceType, Date createdAt) {
        this.id = id;
        this.externalId = externalId;
        this.resourceType = resourceType;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Meta)) {
            return false;
        }
        Meta other = (Meta) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "com.mycompany.mavenproject3.Meta[ id=" + id + " ]";
    }

}
