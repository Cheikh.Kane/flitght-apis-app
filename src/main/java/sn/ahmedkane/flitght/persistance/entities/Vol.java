package sn.ahmedkane.flitght.persistance.entities;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class Vol {
    private Long id;
    private Airline airline;

    @JoinColumn(name = "linked_meta", referencedColumnName = "id", nullable = false)
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    protected Meta linkedMeta;

}
