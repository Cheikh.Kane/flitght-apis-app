package sn.ahmedkane.flitght.persistance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.ahmedkane.flitght.persistance.entities.Meta;
import sn.ahmedkane.flitght.web.dto.MetaDto;

/**
 * @author Ahmed Kane
 */

@Repository
public interface MetaRepository extends JpaRepository<Meta,Long> {

    Meta findByExternalId(String uid);

}
