package sn.ahmedkane.flitght.persistance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.ahmedkane.flitght.persistance.entities.Airport;

import java.util.List;

@Repository
public interface AirportRepository extends JpaRepository<Airport,Long> {

    List<Airport> findByCodeContainingIgnoreCase(String code);
}
