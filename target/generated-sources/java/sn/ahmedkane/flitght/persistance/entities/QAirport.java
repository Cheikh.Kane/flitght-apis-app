package sn.ahmedkane.flitght.persistance.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAirport is a Querydsl query type for Airport
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAirport extends EntityPathBase<Airport> {

    private static final long serialVersionUID = -412076348L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAirport airport = new QAirport("airport");

    public final StringPath city = createString("city");

    public final StringPath code = createString("code");

    public final StringPath country = createString("country");

    public final StringPath countryIso = createString("countryIso");

    public final StringPath county = createString("county");

    public final StringPath iataType = createString("iataType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Double> latitude = createNumber("latitude", Double.class);

    public final QMeta linkedMeta;

    public final NumberPath<Double> longitude = createNumber("longitude", Double.class);

    public final StringPath name = createString("name");

    public final StringPath phone = createString("phone");

    public final StringPath postalCode = createString("postalCode");

    public final StringPath state = createString("state");

    public final StringPath street = createString("street");

    public final StringPath streetNumber = createString("streetNumber");

    public final StringPath time_zone = createString("time_zone");

    public final StringPath uct = createString("uct");

    public final StringPath website = createString("website");

    public QAirport(String variable) {
        this(Airport.class, forVariable(variable), INITS);
    }

    public QAirport(Path<? extends Airport> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAirport(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAirport(PathMetadata metadata, PathInits inits) {
        this(Airport.class, metadata, inits);
    }

    public QAirport(Class<? extends Airport> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.linkedMeta = inits.isInitialized("linkedMeta") ? new QMeta(forProperty("linkedMeta")) : null;
    }

}

